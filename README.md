Custom Rstudio with Docker
---

# Specifications

R version: 3.5.2

OS: CentOS 7 and 8

Default password: pasword
To change it use `-e PASSWORD=<new password>`

See branches for respective Dockerfile.

# Build

``` bash
podman build -t rstudio:3.5.2 .
```
or
``` bash
docker build -t rstudio.3.5.2 .
```

# Run

``` bash
podman run --rm -p 8787:8787 --ulimit=host -d rstudio:3.5.2
```
or
``` bash
docker run --rm -p 8787:8787 -d rstudio:3.5.2
```

To launch with root access, use argument `-e ROOT=TRUE`.

To share a volume use :

```bash
-v /path/to/host/folder:/path/to/container/folder
```

A volume is created in the Dockerfile in `/home/rstudio/data/`.

Some permission problems may be caused by SELinux.
Solve them by adding this rule on the host:

``` bash
chcon -Rt svirt_sandbox_file_t <host folder>
```

# Quay repository

A [Quay repository](quay.io/wellinkstein/rstudio) is available with the latest image build for
centOS 8 and centOS 7.

Run command with Quay:

``` bash
podman run -p 8787:8787 -e ROOT=TRUE --ulimit=host quay.io/wellinkstein/rstudio:centos7
podman run -p 8787:8787 -e ROOT=TRUE --ulimit=host quay.io/wellinkstein/rstudio:centos8
```
or 
``` bash
docker run -p 8787:8787 -e ROOT=TRUE quay.io/wellinkstein/rstudio:centos7
docker run -p 8787:8787 -e ROOT=TRUE quay.io/wellinkstein/rstudio:centos8
```

Other run example:

``` bash
podman run --name rstudio -p 8787:8787 -v ~/shared_volume:/home/rstudio/data/ -e ROOT=TRUE --ulimit=host -d quay.io/wellinkstein/rstudio:centos7
```
